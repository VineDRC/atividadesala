object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Form1'
  ClientHeight = 284
  ClientWidth = 447
  Color = clMaroon
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object DBText1: TDBText
    Left = 240
    Top = 24
    Width = 137
    Height = 17
    DataSource = DataModule2.DataSourcePokemon
  end
  object Label2: TLabel
    Left = 159
    Top = 47
    Width = 58
    Height = 32
    Caption = 'id_treinador'
  end
  object Label3: TLabel
    Left = 159
    Top = 66
    Width = 26
    Height = 13
    Caption = 'nome'
  end
  object Label4: TLabel
    Left = 159
    Top = 96
    Width = 22
    Height = 13
    Caption = 'nivel'
  end
  object identificador: TLabel
    Left = 159
    Top = 24
    Width = 59
    Height = 13
    Caption = 'identificador'
  end
  object DBText2: TDBText
    Left = 240
    Top = 47
    Width = 137
    Height = 17
    DataSource = DataModule2.DataSourcePokemon
  end
  object DBText3: TDBText
    Left = 240
    Top = 70
    Width = 137
    Height = 17
    DataSource = DataModule2.DataSourcePokemon
  end
  object DBText4: TDBText
    Left = 240
    Top = 96
    Width = 137
    Height = 13
    DataSource = DataModule2.DataSourcePokemon
  end
  object bu: TButton
    Left = 8
    Top = 175
    Width = 75
    Height = 25
    Caption = 'Editar'
    TabOrder = 0
    OnClick = buClick
  end
  object Button1: TButton
    Left = 112
    Top = 175
    Width = 75
    Height = 25
    Caption = 'Inserir'
    TabOrder = 1
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 214
    Top = 175
    Width = 75
    Height = 25
    Caption = 'Deletar'
    TabOrder = 2
    OnClick = Button2Click
  end
  object DBLookupListBox1: TDBLookupListBox
    Left = 8
    Top = 8
    Width = 145
    Height = 134
    DataField = 'nome'
    DataSource = DataModule2.DataSourcePokemon
    ListSource = DataModule2.DataSourcePokemon
    TabOrder = 3
  end
end
